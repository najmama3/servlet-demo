# Simplistic Servlet Demo

Simple demo of annotation-configured Servlet and Filter usage.

Tests are run using Jetty. Application is packaged as WAR and can be deployed to an application server.

## Repository

* [https://gitlab.fel.cvut.cz/ear/servlet-demo](https://gitlab.fel.cvut.cz/ear/servlet-demo)

## License

LGPLv3
