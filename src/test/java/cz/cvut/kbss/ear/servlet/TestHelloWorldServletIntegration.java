package cz.cvut.kbss.ear.servlet;

import org.eclipse.jetty.http.HttpStatus;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.FilterHolder;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.servlet.DispatcherType;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.EnumSet;
import java.util.stream.Collectors;

class TestHelloWorldServletIntegration {

    private static final int PORT = 8090;

    private final String ctx = "hello-test";

    private Server getTestServer() {
        final Server server = new Server(PORT);
        // Create Server
        ServletContextHandler context = new ServletContextHandler();
        ServletHolder servlet = new ServletHolder("hello", HelloWorldServlet.class);
        // @WebServlet annotation is not processed, see https://stackoverflow.com/a/13955415
        context.addServlet(servlet, "/" + ctx);
        servlet.setInitParameter("brand", "KBSS");

        FilterHolder filter = new FilterHolder(LocalhostServletFilter.class);
        context.addFilter(filter, "/*", EnumSet.allOf(DispatcherType.class));
        server.setHandler(context);
        return server;
    }

    @Test
    void testWebGet() throws Exception {
        final Server server = getTestServer();
        server.start();
        final HttpURLConnection http
                = (HttpURLConnection) new URL("http://localhost:" + PORT + "/" + ctx).openConnection();
        http.connect();

        // Check status
        Assertions.assertEquals(HttpStatus.OK_200, http.getResponseCode());

        // Check response text
        final BufferedReader r = new BufferedReader(new InputStreamReader(http.getInputStream()));
        final String x = r.lines().collect(Collectors.joining(","));
        Assertions.assertTrue(x.startsWith("HELLO"));
        // to test the running server, uncomment this line and put a breakpoint on the server
        // .stop() line
        // server.join();
        server.stop();
    }
}
