package cz.cvut.kbss.ear.servlet;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.junit.jupiter.api.Test;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import static cz.cvut.kbss.ear.servlet.HelloWorldServlet.JSON_MIME_TYPE;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class TestHelloWorldServlet {

    @Test
    void testGetWithoutAccept() throws IOException {
        final HttpServletRequest request = mock(HttpServletRequest.class);
        final HttpServletResponse response = mock(HttpServletResponse.class);
        final StringWriter w = new StringWriter();
        when(response.getWriter()).thenReturn(new PrintWriter(w));
        new HelloWorldServlet().doGet(request, response);
        w.flush();

        assertTrue(w.toString().startsWith("HELLO"));
    }

    @Test
    void testGetWithJsonAccept() throws Exception {
        final HttpServletRequest request = mock(HttpServletRequest.class);
        final HttpServletResponse response = mock(HttpServletResponse.class);

        final StringWriter w = new StringWriter();
        when(response.getWriter()).thenReturn(new PrintWriter(w));
        when(request.getHeader("Accept")).thenReturn(JSON_MIME_TYPE);
        new HelloWorldServlet().doGet(request, response);
        w.flush();

        final JSONParser p = new JSONParser();
        final Object o = p.parse(w.toString());

        assertTrue(o instanceof JSONObject);
        final JSONObject oo = (JSONObject) o;
        assertTrue(oo.get("message").toString().startsWith("hello"));
    }
}
