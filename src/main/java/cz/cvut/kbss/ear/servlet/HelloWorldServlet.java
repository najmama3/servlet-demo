package cz.cvut.kbss.ear.servlet;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = {"/hello/*"}, initParams = {
        @WebInitParam(name = "brand", value = "KBSS"),
})
public class HelloWorldServlet extends HttpServlet {

    static final String JSON_MIME_TYPE = "application/json";
    private static final String TEXT_MIME_TYPE = "text/plain";

    private static int count = 0;

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        System.out.println("This servlet has been created by " + getInitParameter("brand"));
    }

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        // create session - puts JSESSIONID into cookies
        // req.getSession(true);

        final String accept = req.getHeader("Accept");

        final String me = req.getQueryString();
        if (accept != null && accept.equals(JSON_MIME_TYPE)) {
            resp.setContentType(JSON_MIME_TYPE);
            resp.getWriter().write("{ \"message\" : \"hello " + me + "\", \"count\" : \"" + count + "\"}");
        } else {
            resp.setContentType(TEXT_MIME_TYPE);
            resp.getWriter().write("HELLO " + me + ", count=" + count);
        }
        count++;
    }

    public void destroy() {
        super.destroy();
        System.out.println("This servlet is closing down.");
    }
}
