package cz.cvut.kbss.ear.servlet;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;

@WebFilter(filterName = "Only localhost requests", urlPatterns = {"/*"})
public class LocalhostServletFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) {
        System.out.println("Initializing the filter " + getClass().getName());
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse,
                         FilterChain filterChain) throws IOException, ServletException {
        System.out.println("Filtering using the filter " + getClass().getName());
        final String addr = servletRequest.getLocalAddr();
        if (addr.matches("localhost") || addr.matches("127.0.0.1") || addr
            .matches("0:0:0:0:0:0:0:1")) {
            System.out.println("- match, continuing.");
            if (servletRequest instanceof HttpServletRequest) {
                final HttpServletRequest req = (HttpServletRequest) servletRequest;
                if (!"myself".equals(req.getQueryString())) {
                    filterChain.doFilter(servletRequest, servletResponse);
                } else {
                    System.out.println("No hello to myself.");
                    throw new RuntimeException();
                }
            }
        } else {
            System.out.println("Processing skipped, returning.");
        }
    }

    @Override
    public void destroy() {
        // Do nothing
    }
}
